class UserInteraction {
        
    static howToPlay() {
        console.log(`To play just use: ticTacToeGame.play(x, y).\n\n`);
    }

    static welcome() {
        console.log(`Welcome to Tic-Tac-Toe-Console-Edition`);
    }

    static nextPlayerTurn(currentPlayer) {
        console.log(`It's your turn player ${currentPlayer}`);
    }

    static gameEndedWithNoWinner() {
        console.log(`Game ended. No winner tho..`);
    }

    static detailsAboutUpdatingConfiguration() {
        console.log("You can reset the game and update the current configuration by running either (at any time):\n" +
                    "ticTacToeGame.readConfigurationAsJson() with a JSON like {\"size\": 5, \"shapes\": [\"x\",\"y\",\"z\"], \"players\": 3}\n" +
                    "ticTacToeGame.readConfigurationFromAddressBarAsParams() for params like ?size=4 or &shapes=[\"x\",\"y\",\"z\"] or &players=4\n" +
                    "ticTacToeGame.readConfigurationAsJsonFromURL('http[s]://metronom') where the content is JSON compliant\n");
    }

    static winningResult(typeOfLine, line) {
        console.log(`Winning ${typeOfLine} detected.`);
        if(line !== undefined) {
            console.log(`Line that matched was: ${line}`);
        }
    }

    static notAcceptingMoreMoves() {
        console.log(`Game Over. New set of moves will not be considered and played`);
    }


    static gameOver(currentPlayer) {
        console.log(`Game Over. Winner is player ${currentPlayer}`);
    }

    static welcomeUser() {
        this.welcome();
        this.detailsAboutUpdatingConfiguration();
        this.howToPlay();
    }
}

if (typeof module !== 'undefined' && module.exports) {
    module.exports = UserInteraction;
}
