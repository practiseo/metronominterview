class VerifyWinner {

    constructor (matrix, userInteraction) {
        this.matrix = matrix;
        this.userInteraction = userInteraction; 
    } 

    /**
     * check if there is a winner
     */
    anyWinner() {
        if(this.anyWinningRow() || this.anyWinningColumn() || this.anyWinningDiagonal()) {
            this.userInteraction.gameOver(this.matrix.getCurrentPlayer());
            this.matrix.declareGameOver();
            return true;
        }
        return false;
    }

    /**
     * check if any row is a winner combination
     */
    anyWinningRow() {
        for(var r = 0; r < this.matrix.getTicTacToeSize(); r++) {
            if( this.matrix.isRowWinning(r)) {
                this.userInteraction.winningResult("LINE", r)
                return true;
            }
        }
        return false;
    }

    getMatrixDataStore() {
        return this.matrix.getDataStore();
    }

    /**
     * check if any column is a winner combination
     */
    anyWinningColumn() {
        for(var c = 0; c < this.matrix.getTicTacToeSize(); c++) {
            if( this.matrix.isColumnWinning(c)) {
                this.userInteraction.winningResult("COLUMN", c)
                return true;
            }
        }
        return false;
    }

    /**
     * check if any diagonal is a winner combination
     */
   anyWinningDiagonal() {
       if(this.matrix.isLeftBottomRightTopDiagonalWinning() || this.matrix.isLeftTopRightDownDiagonalWinning()) {
            this.userInteraction.winningResult("DIAGONAL")
            return true;
       } 
       return false;
   }
}

if (typeof module !== 'undefined' && module.exports) {
    module.exports = VerifyWinner;
}