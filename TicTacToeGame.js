class TicTacToeGame {

    constructor (matrix, verifyWinner, userInteraction, blocks) {
        this.matrix = matrix;
        this.verifyWinner = verifyWinner;
        this.userInteraction = userInteraction;
        this.blocks = blocks;
    }

    /**
     * sample format {"size": 5, "shapes": ["x","y","z"], "players": 3}
     * @param {String} json
     */
    readConfigurationAsJson(json) {
        var parsedJson = JSON.parse(json);
        var objectIsValid = true;

        let configuration = this.matrix.getTicTacToeConfigurationObject();

        Object.keys(configuration).forEach(function(key) {
            if (!parsedJson.hasOwnProperty(key)) {
                objectIsValid = false;
                return;
            }
        });

        if(parsedJson.shapes.length != parsedJson.players) {
            objectIsValid = false;
        }

        if (!objectIsValid) {
            console.log(`Invalid configuration offered. Make sure your data has these keys correct: ${Object.keys(configuration).join(",")}`);
            return;
        }

        this.matrix.updateTicTacToeConfigurationObject(parsedJson);

        console.log(`Configuration parsed correctly. Here is the new configuration: ${JSON.stringify(configuration)}`);

        this.resetTheBoard();
    }

    /**
     * just add to the url some params, like ?size=4 or &shapes=["x","y","z"] or &players=4
     */
    readConfigurationFromAddressBarAsParams() {
        var params = {};
        var needToUpdateBoard = false;

        if (location.search) {
            var parts = location.search.substring(1).split('&');

            for (var i = 0; i < parts.length; i++) {
                var param = parts[i].split('=');
                if (!param[0])
                    continue;

                if (Object.keys(this.configuration).includes(param[0])) {
                    var parsedPayload = JSON.parse(decodeURIComponent(param[1]));
                    this.configuration[param[0]] = parsedPayload;
                    console.log(`Updated configuration for key <${param[0]}> with the value: ${decodeURIComponent(param[1])}`);
                    needToUpdateBoard = true;
                }
            }
        }

        if(needToUpdateBoard) {
            this.resetTheBoard();
        }
    }

    /**
     * just pass a GET'[t]able file that is a valid JSON with the needed values
     * {@link https://gist.githubusercontent.com/practiseo/1903425b4ca365fbd9e5fc24230d9c2b/raw/e9a8a71a7ba01e7a1fb678e4c650686242cb66b3/test}
     */
    readConfigurationAsJsonFromURL(url) {
        var ajaxRequest;
        var self = this;

        try {
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    console.log(`Your browser needs to be updated!`);
                    return false;
                }
            }
        }

        ajaxRequest.onreadystatechange = function() {
            if (ajaxRequest.readyState == 4) {
                self.readConfigurationAsJson(ajaxRequest.responseText);
            }
        }

        ajaxRequest.open("GET", url, true);
        ajaxRequest.send(null);
    }

    
    /**
     * Players turn. Coords given x for row, y for column
     * @param {number} x
     * @param {number} y
     */
    playNextBlock(x, y) {
        if (isNaN(x) || isNaN(y)) {
            console.log("Please input valid numbers, like \"1\", \"2\". Please pick valid numbers player " + getCurrentPlayer());
            return false;
        }

        return this.updateUIBlockAndStore(x, y);
    }

    getDataStore() {
        return this.matrix.getDataStore();
    }

    /**
     * It's computers turn
     */
    computerPlayNextBlock() {
        console.log("Computer play time");
        var nextCellObject = this.matrix.fillNextEmptyCell();
        return this.updateUIBlockAndStore(nextCellObject.x + 1, nextCellObject.y + 1);
    }

    /**
     * Update the data model and the UI with the players move
     */
    updateUIBlockAndStore(x, y) {

        if (!this.matrix.isCellEmpty(x - 1, y - 1)) {
            console.log(`Cell already played for coords: ${x}x${y}. Please pick another cell player ${this.matrix.getCurrentPlayer()}`);
            return false;
        }

        this.matrix.updateCell(x - 1, y - 1);
        

        var getCell = this.blocks.getDOMElement(x, y);
        if(getCell) {
            this.blocks.updateCellWithSymbol(getCell);
        }

        console.log(`Confirmed move at ${x}x${y} for player ${this.matrix.getCurrentPlayer()}`);
        this.matrix.displayCurrentBoard();
        return true;
    }

    startGame() {
        this.matrix.displayCurrentBoard();
        if (this.matrix.isComputerTurn()) {
            this.play();
        } else {
            this.userInteraction.nextPlayerTurn(this.matrix.getCurrentPlayer());   
        }
    }

    /**
     * the game main CTA
     */
    play(x, y) {
        if(this.matrix.isGameOver()) {
            this.userInteraction.notAcceptingMoreMoves();
            return;
        }

        if (x !== undefined && y !== undefined && this.playNextBlock(x, y)) {
            if(!this.verifyWinner.anyWinner()) {
                this.matrix.nextPlayer(); 
            }
        }

        if (this.matrix.isComputerTurn() && this.matrix.anyEmptyCellLeft()) {
            if (this.computerPlayNextBlock()) {
                if(!this.verifyWinner.anyWinner()) {
                    this.matrix.nextPlayer();    
                }
                
            }
        }     
        
        // if no winners...then invite the next player
        if(!this.matrix.isGameOver()) {
            this.userInteraction.nextPlayerTurn(this.matrix.getCurrentPlayer());
        }

        // if all matrix is full and no winner condition tripped so far..stop it either way
        if(!this.matrix.anyEmptyCellLeft()) {
            this.matrix.declareGameOver();
            this.userInteraction.gameEndedWithNoWinner();    
            return false;
        }

        return true;
    }
     
    /**
     * Reset everything and start a game;
     */
    resetTheBoard() {
        this.matrix.pickRandomUserToStart();
        this.blocks.recreatematrix();
        this.startGame();
    }

    /**
     * Play random game alone
     */
    playGame() {
        var count = this.matrix.getTicTacToeSize() -1;
        for(var sim = 0; sim < ((count*count) + 5); sim++) {
            this.play(Math.round(Math.random() * count + 1) , Math.round(Math.random() * count + 1));
        }
    }

    /**
     * let the computer play alone for some games to show it works repeatedly
     */
    playAFewGamesAlone() {
        for(var i = 0; i < 10; i++)  {
            console.log(`\n\n --- start of game ----`);
            this.resetTheBoard(); 
            this.playGame();
            console.log(`\n\n --- end of game ----`);
        }
    }

    /**
     * lead the game into the direction of having a winner by making a vertical of the same kind
     */
    playGameVerticalWinner() {
        this.play(3,1);
        this.play(1,3);
        this.play(3,2);
        this.play(2,3);
        this.play(3,3);
    }

    /**
     * lead the game into the direction of having a winner by making a line of the same kind
     */
    playGameLineWinner() {
        this.play(1,3);
        this.play(3,1);
        this.play(2,3);
        this.play(2,1);
        this.play(3,3);
    }

    /**
     * lead the game into the direction of having a winner by making a diagonal of the same kind
     */
    playGameDiagonalWinner() {
        this.play(3,1);
        this.play(3,3);
        this.play(1,3);
        this.play(2,3);
        this.play(2,2);
    }

    /**
     * play a game in which predictably the AI wins. Skynet wins...
     */
    winnerIsAI() {
        this.play(3,1);
        this.play(3,2);
        this.play(2,2);
        this.play(2,1);
        this.play(3,3);
        this.play(2,3);
    }    
}

if (typeof module !== 'undefined' && module.exports) {
    module.exports = TicTacToeGame;
}