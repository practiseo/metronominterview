class Blocks {

    constructor(matrix) {
        this.matrix = matrix;       
    }

    /**
     * @param {number} iteratorX
     * @param {number} rangeY
     */
    createColumn(iteratorX, rangeY) {
        var newRow = document.createElement("div");
        newRow.className = 'xoColumn';
    
        for (var cell = 0; cell < rangeY; cell++) {
            var newBlock = this.createBlock(iteratorX, cell);
            newRow.appendChild(newBlock);
        }
        return newRow;
    }

    /**
     * @param {number} coordX
     * @param {number} coordY
     */
    createBlock(coordX, coordY) {
        var newBlock = document.createElement("div");
        newBlock.className = 'xoBlock';
        newBlock.id = `${coordX}_${coordY}`;
        newBlock.innerText = "";
    
        return newBlock;
    }
    
    /**
     * @param {number} coordX
     * @param {number} coordY
     */
    getDOMElement(coordX, coordY) {
        return document.getElementById((coordX - 1) + "_" + (coordY - 1));
    }

    /**
     * @param {webElement} domElement
     */
    updateCellWithSymbol(domElement) {
         domElement.innerText = this.matrix.getCurrentPlayerSymbol();;
    }

    createMatrixContainer(matrixSize) {
        var matrixContainer = document.createElement("div");

        for (var iteratorY = 0; iteratorY < matrixSize; iteratorY++) {
            var newRowWithBlocks = this.createColumn(iteratorY, matrixSize);
            matrixContainer.appendChild(newRowWithBlocks);
        }

        matrixContainer.id = "matrixContainer";
        matrixContainer.className = "matrixContainer";

        return matrixContainer;
    }

    /**
     * Create a square matrix of a variable number of cells x*x
     * @param {string} configurationSize
     */
    creatematrix(configurationSize) {
        let matrixContainer = this.createMatrixContainer(configurationSize);

        this.matrix.purePopulateStore(configurationSize);  // populate the data store
        this.matrix.declareGameStarted();
            
        return matrixContainer;
    }

    /**
     * Updates the number of players
     * @param {string} players
     */
    updateNumberOfPlayers(players) {
        var numberOfPlayers = document.getElementById("numberOfPlayers");
        if(numberOfPlayers) {
            numberOfPlayers.innerText = players;
        }
    }

    /**
     * Deletes the current matrix
     */
    deletematrix() {
        var matrixContainer = document.getElementById("matrixContainer");
        if(matrixContainer) {
            matrixContainer.parentNode.removeChild(matrixContainer);            
        }
    }

    /**
     * Wrapper method to delete the current matrix (if any) and create a new one
     */
    recreatematrix() {
        var configuration = this.matrix.getTicTacToeConfigurationObject();
        this.deletematrix();
        this.initmatrix(configuration);
        console.log(`[Re]Created a matrix of ${configuration.size}*${configuration.size} size`);
    }
    
    /**
     * Add the newly created matrix container to the page
     */ 
    addMatrixContainerToPage(matrixContainer) {
        var bodyElement = document.getElementsByTagName("body")[0];
        bodyElement.appendChild(matrixContainer);
    }

    /**
     * Get the body element and into it insert the new dom structure generated
     */
    initmatrix() {
        var configuration = this.matrix.getTicTacToeConfigurationObject();
        
        let createdMatrixContainer = this.creatematrix(configuration.size);

        this.addMatrixContainerToPage(createdMatrixContainer);
        this.updateNumberOfPlayers(configuration.players);
    }
}

if (typeof module !== 'undefined' && module.exports) {
    module.exports = Blocks;
}