## GIVEN TASK

We want to bring the pen-and-paper game Tic-tac-toe to the digital age,
but with a little twist: the size of the playfield should be
configurable between 3x3 and 10x10. And we also want the symbols
(usually O and X) to be configurable. Also it should be for 3 players
instead of just 2.

General Rules: https://en.wikipedia.org/wiki/Tic-tac-toe

The two users will play against each other and against the computer.
Who is starting is random. In and output should be on the console.
After each move, the new state of the playfield is displayed and the
player can enter the next position of their character one after
another. The next position should be provided in a format like 3,2.
Invalid inputs are expected to be handled appropriately.

Requirements:
 - Use the programming language you feel most comfortable with
 - The game takes 3 inputs:
 - Size of the playground. Valid values are between 3 and 10.
 - Play character 1, 2 and 3:
 - A single character for the human player 1
 - A single character for the human player 2
 - A single character for the computer
 - These configurations should come from a file
 - Software design is more important than a highly developed AI
 - Please put the completed assignment on GitHub.

Rules:
- You may use external libraries only for testing or building purposes
e.g. JUnit, Gradle, Rspec, Rake, GulpJS, etc.
 - Please provide an explanation how to run your code
 - Please explain your design decisions and assumptions
 - Don't include executables* in your submission.
 - Please write your solution in a way, that you would feel comfortable
handing this over to a colleague and deploy it into production.
 - We especially look at design aspects (e.g. OOP) and if the code is
well tested and understandable.
 - this includes: asp, bat, class, cmd, com, cpl, dll, exe, fon, hta,
ini, ins, iw, jar, jsp, js, jse, pif, scr, shs, sh, vb, vbe, vbs, ws,
wsc, wsf, wsh & msi


## Parsed requirements/constraints
- game has a variable number of players 
- game accepts a custom set of symbols for representing players
- game is configured to work with 3 players (where the last player is the computer)
	 - game actually accepts any Int as number of players. Last inclusive player is the computer
 - users turn is cyclic
 - randomly a player or the computer starts the game
	 - if the computer is the first one, he already moves
- all progress and info is displayed in the console. here the Developer Console
- after each step the field is displayed with the new state of the board
- invalid inputs are handled properly

## Running application

- Download as an archive the app or git clone it
- Go into the folder of the app
- Run in your browser the file called ```interview.html```.
- Open the Developer Tools and look at the console tab
- Rest is self explanatory

**RESTRICTION**: modern browser, JavaScript enabled (min ES5)

## Running tests

- Download as an archive the app or git clone it
- Go into the folder of the app
- Run in your browser the files called like ```.*.Spec.html``` or the file[s] in the folder integration.
- Rest is self explanatory

### Alternatively

- While running the application the tests links are availabe in the UI as a POC.

**RESTRICTION**: modern browser, JavaScript enabled (min ES5)

**RESTRICTION**: unrestricted network access/firewall to public CDN's for library download

## Simulating deploy to production

Due to the simplicity of the app, the lack of packaging, or obfuscation, here is a simple Heroku tutorial on how to get it running like here:
https://metronominterview.herokuapp.com/interview.html

#### Steps
- Login into your heroku account
- Create a new dyno instance and link it to a fresh/old git folder
- Define for the dyno that it will be running with a PHP buildpack (Settings > Buildpacks > Add Buildpack)
- Copy over the source code give; Notice there is a empty composer.json there with a barebones configuration.
- Push the code to heroku
- Expect the build to be a success
- Open your browser to https://{$yourProject}.herokuapp.com/interview.html

## Why some of the decisions ?

- #### Why javascript (.js) file when the task forbids .js?
Well, because there .js referes to the processed edition of the files, hard to read. But like .java vs .class, here i present the .js files as sources, not packaged files.

- #### Why no node.js and ES6 ? 
Why complicated when simple is also feasible for the complexity of the given project. No real need for ES6 complexities (node>babel>..), and no Node specific API really needed.

- #### Why testing in browser? 
Because there is no need of complicated setup, no runners in sight, no need to report the result anywhere or generate a results.html report or so..

- #### Why not Github but BitBucket?
Because my private account is bound to work projects also. I would not want this interview application to be included into my public code.

## Application structure

- `readme.md` - the file we looking at now
- `styles.css` - the css file for helping with positioning the elements in flexboxes, aligning the text, font sizes and general centering
- `interview.html` - the **start file** where 
- `Blocks.js` - the class that takes care of reading/updating and generating the tic tac toe game UI elements. It's a dom manipulation class
- `Matrix` - the class handling all the game parameters, and data evolution
- `TicTacToeGame.js` - the main class with the play steps and sample games and configuration reading/parsing
- `UserInteraction.js` - the file explicitly saved for strings. Further it should/could/would be expanded as the starting point to an i18n application
- `VerifyWinner.js` - class the verify the condition of winning. Checks are done by rows, columns and diagonals

## Sample command lines

When running the application you will be welcomed by some guidance code; The game starts by reading a predefined configuration and decides on a random player to start. If it's the last player, decidedly to be the computer AI player, then already the first move will be made

```
Welcome to Tic-Tac-Toe-Console-Edition
You can reset the game and update the current configuration by running either (at any time):
ticTacToeGame.readConfigurationAsJson() with a JSON like {"size": 5, "shapes": ["x","y","z"], "players": 3}
ticTacToeGame.readConfigurationFromAddressBarAsParams() for params like ?size=4 or &shapes=["x","y","z"] or &players=4
ticTacToeGame.readConfigurationAsJsonFromURL('http[s]://metronom') where the content is JSON compliant

To play just use: ticTacToeGame.play(x, y). Now playing player 0 
```

### Give a custom configuration

- #### ticTacToeGame.readConfigurationAsJson() 

Offer a JSON configuration, like {{'{"size": 5, "shapes": ["x","y","z"], "players": 3}'}}

- #### ticTacToeGame.readConfigurationFromAddressBarAsParams()

Offer a query param configuration, like {{?size=4 or &shapes=["x","y","z"] or &players=4}}

- #### ticTacToeGame.readConfigurationAsJsonFromURL()

Offer an URL from where the application can read a JSON compliant file. I offer a sample link here like this:
ticTacToeGame.readConfigurationAsJsonFromURL("https://gist.githubusercontent.com/practiseo/1903425b4ca365fbd9e5fc24230d9c2b/raw/e9a8a71a7ba01e7a1fb678e4c650686242cb66b3/test")


### Play a cell

```
ticTacToeGame.play(2, 3) // valid cell
ticTacToeGame.play(55, 55) // invalid cell
```

If the cell is not found because the coordinates are outside the range specified, then the move of the player is rejected and a dedicated message is returned like ```Cell not found at coords: 55x55. Please pick another cell player 1```

If the cell is found, the action is taken into consideration and the new board is updated. "Him" is the symbol assigned to the player 1. The list is looking like "You, Him, Me, AI...".

```
ticTacToeGame.play(1, 1)
Confirmed move at 1x1 for player 1
| Him        |            |            |
|            |            |            |
|            |            |            |
```

###  Reset the board

```
ticTacToeGame.resetTheBoard()
```

Will empty the data and update the UI;
Result will look similar to this

```
Recreated a matrix of 3*3 size
It's your turn player 0
|            |            |            |
|            |            |            |
|            |            |            |
It's your turn player 0
```

### Random Scenarios

```
ticTacToeGame.playAFewGamesAlone();
```

The game will reset the board, pick a new random user, (in case it's the AI - it will make the first predictable step), then forcefully a maximum of 15 valid steps will be played. Games will happy by looping the players and for each player's turn giving random values for X and Y. Known fact, the values are not stored, but collisions are desired to show handling of cases for played cells.

Sample game

```
 --- start of game ----
Recreated a matrix of 3*3 size
It's your turn player 1
|            |            |            |
|            |            |            |
|            |            |            |
Winning COLUMN detected.
Line that matched was: 0
Game Over. Winner is player 2
Game Over. New set of moves will not be considered and played
 --- end of game ----
```
 
### Predefined Winner scenarios

#### Vertical line

```
ticTacToeGame.playGameVerticalWinner();
```

Will play by itself and log all the steps like so:

```
Confirmed move at 3x1 for player 0
|            |            | You        |
|            |            |            |
|            |            |            |
It's your turn player 1
Confirmed move at 1x3 for player 1
|            |            | You        |
|            |            |            |
| Him        |            |            |
Computer play time
Confirmed move at 1x1 for player 2
| AI         |            | You        |
|            |            |            |
| Him        |            |            |
It's your turn player 0
Confirmed move at 3x2 for player 0
| AI         |            | You        |
|            |            | You        |
| Him        |            |            |
It's your turn player 1
Confirmed move at 2x3 for player 1
| AI         |            | You        |
|            |            | You        |
| Him        | Him        |            |
Computer play time
Confirmed move at 1x2 for player 2
| AI         |            | You        |
| AI         |            | You        |
| Him        | Him        |            |
It's your turn player 0
Confirmed move at 3x3 for player 0
| AI         |            | You        |
| AI         |            | You        |
| Him        | Him        | You        |
Winning COLUMN detected.
Line that matched was: 2
Game Over. Winner is player 0
```

#### Line Winner

```
ticTacToeGame.playGameLineWinner()
```

Will play by itself and log all the steps like so:

```
Confirmed move at 1x3 for player 1
|            |            |            |
|            |            |            |
| Him        |            |            |
Computer play time
Confirmed move at 1x1 for player 2
| AI         |            |            |
|            |            |            |
| Him        |            |            |
It's your turn player 0
Confirmed move at 3x1 for player 0
| AI         |            | You        |
|            |            |            |
| Him        |            |            |
It's your turn player 1
Confirmed move at 2x3 for player 1
| AI         |            | You        |
|            |            |            |
| Him        | Him        |            |
Computer play time
Confirmed move at 1x2 for player 2
| AI         |            | You        |
| AI         |            |            |
| Him        | Him        |            |
It's your turn player 0
Confirmed move at 2x1 for player 0
| AI         | You        | You        |
| AI         |            |            |
| Him        | Him        |            |
It's your turn player 1
Confirmed move at 3x3 for player 1
| AI         | You        | You        |
| AI         |            |            |
| Him        | Him        | Him        |
Winning LINE detected.
Line that matched was: 2
Game Over. Winner is player 1
```

#### Diagonal Winner

```
ticTacToeGame.playGameDiagonalWinner()
```

Will play by itself and log all the steps like so:

```
Confirmed move at 3x1 for player 1
|            |            | Him        |
|            |            |            |
|            |            |            |
Computer play time
Confirmed move at 1x1 for player 2
| AI         |            | Him        |
|            |            |            |
|            |            |            |
It's your turn player 0
Confirmed move at 3x3 for player 0
| AI         |            | Him        |
|            |            |            |
|            |            | You        |
It's your turn player 1
Confirmed move at 1x3 for player 1
| AI         |            | Him        |
|            |            |            |
| Him        |            | You        |
Computer play time
Confirmed move at 1x2 for player 2
| AI         |            | Him        |
| AI         |            |            |
| Him        |            | You        |
It's your turn player 0
Confirmed move at 2x3 for player 0
| AI         |            | Him        |
| AI         |            |            |
| Him        | You        | You        |
It's your turn player 1
Confirmed move at 2x2 for player 1
| AI         |            | Him        |
| AI         | Him        |            |
| Him        | You        | You        |
Winning DIAGONAL detected.
Game Over. Winner is player 1
```