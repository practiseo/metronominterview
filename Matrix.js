class Matrix {
    constructor () {
        this.dataStore = {
            cells: [[]],
            currentPlayer: 0,
            gameOver: false
        }
        this.configuration = {
            size: 3,
            shapes: ['A', 'B', 'C'],
            players: 3
        };
    }

    getDataStore () {
        return this.dataStore;
    }

    displayCurrentBoard() {
        for(var b = 0; b < this.dataStore.cells.length; b++) {
            var outputLine = `|`;
            for(var c = 0; c < this.dataStore.cells[b].length; c++) {
                outputLine += ` ${this.dataStore.cells[b][c].padEnd(10," ")} |`;
            }
            console.log(outputLine);
        }
    }

    declareGameOver() {
        this.dataStore.gameOver = true;
    }

    declareGameStarted() {
        this.dataStore.gameOver = false;
    }

    pickRandomUserToStart() {
        this.dataStore.currentPlayer = Math.round(Math.random() * (this.getTicTacToePlayerCount() - 1));
    }

    isGameOver() {
        return this.dataStore.gameOver;
    }

    updateCell(x, y, shape) {
        this.dataStore.cells[y][x] = (shape !== undefined ? shape : this.getCurrentPlayerSymbol());
    }

    getCurrentPlayerSymbol() {
        return (this.getTicTacToeShapes())[this.getCurrentPlayer()];
    }

    getCurrentPlayer() {
        return this.dataStore.currentPlayer;
    }
    
    isComputerTurn() {
        return this.dataStore.currentPlayer == this.getTicTacToePlayerCount() - 1;
    }
    
    nextPlayer() {
        this.dataStore.currentPlayer++;
        
        if (this.dataStore.currentPlayer == this.getTicTacToePlayerCount()) {
        this.dataStore.currentPlayer = 0;
        }
    }
  
    /**
     *    * .. *
     */
    isRowWinning(x) {

        let dataStore = this.getDataStore();

        var reference = dataStore.cells[x][0];

        if(reference === "") {
            return false;
        }

        for (var i = 0; i < dataStore.cells[x].length; i++) {
            if (dataStore.cells[x][i] !== reference) {
                return false;
            }
        }
        return true;
    }

    /**
     *    *
     *    ..
     *    *
     */
    isColumnWinning(x) {

        let dataStore = this.getDataStore();

        var reference = dataStore.cells[0][x];

        if(reference === "") {
            return false;
        }
        
        for (var i = 0; i < dataStore.cells[x].length; i++) {
            if (dataStore.cells[i][x] !== reference) {
                return false;
            }
        }
        return true;
    }

    /**
     *    *
     *     ..
     *       *
     */
    isLeftTopRightDownDiagonalWinning() {

        let dataStore = this.getDataStore();

        var reference = dataStore.cells[0][0];

        if(reference === "") {
            return false;
        }

        for (var i = 0; i < this.getTicTacToeSize(); i++) {
            for (var j = 0; j < this.getTicTacToeSize(); j++) {
                if (dataStore.cells[i][j] !== "" || (i == j && !this.isCellEmpty(i, j) && dataStore.cells[i][j] !== reference)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     *       *
     *     ..
     *   *
     */
    isLeftBottomRightTopDiagonalWinning() {
      
        let dataStore = this.getDataStore();

        var reference = dataStore.cells[this.getTicTacToeSize() - 1][0];

        if(reference === "") {
            return false;
        }
        
        for (var i = this.getTicTacToeSize() - 1; i >= 0; i--) {
            for (var j = 0; j <= this.getTicTacToeSize(); j++) {
                if (i + j == this.getTicTacToeSize() - 1) {
                    if (dataStore.cells[i][j] != reference) {
                        return false;
                    }
                }
            }
        }
        return true;
    }


    isCellEmpty(x, y) {
        let dataStore = this.getDataStore();
        return dataStore.cells[y][x] == "";
    }
    
    purePopulateStore(size) {
        var rowArray = new Array(size);
        for (var x = 0; x < size; x++) {
            var columnArray = new Array(size);
            for (var y = 0; y < size; y++) {
                columnArray[y] = "";
            }
            rowArray[x] = columnArray;
        }
    this.dataStore.cells = rowArray;
    }
    
    fillNextEmptyCell() {
        let dataStore = this.getDataStore();
        for (var i = 0; i < dataStore.cells.length; i++) {
            for (var j = 0; j < dataStore.cells.length; j++) {
                if (this.isCellEmpty(i, j)) {
                    return {"x": i, "y": j};
                }
            }
        }
    }

    anyEmptyCellLeft() {
        let dataStore = this.getDataStore();
        for (var i = 0; i < dataStore.cells.length; i++) {
            for (var j = 0; j < dataStore.cells.length; j++) {
                if (this.isCellEmpty(i, j)) {
                    return true;
                }
            }
        }
        return false;
    }
 
    /**
     * return the configuration value for the size of the matrix
     */
    getTicTacToeSize() {
        return this.configuration.size;
    }

    /**
     * return the configuration value array for the shapes
     */
    getTicTacToeShapes() {
        return this.configuration.shapes;
    }

    /**
     * return the configuration value for the player count
     */
    getTicTacToePlayerCount() {
        return this.configuration.players;
    } 

    /**
     * return the configuration object
     */
    getTicTacToeConfigurationObject() {
        return this.configuration;
    }

    /**
     * return the configuration object
     */
    updateTicTacToeConfigurationObject(configuration) {
        this.configuration = configuration;
    }
}

if (typeof module !== 'undefined' && module.exports) {
    module.exports = Matrix;
}